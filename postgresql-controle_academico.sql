-- Um sistema para controle acadêmico de uma faculdade.

CREATE TABLE departamento(
	id_dep SERIAL PRIMARY KEY,
	nome_dep VARCHAR(50)
);

INSERT INTO departamento(nome_dep)
VALUES
('Setor De Ciencias Naturais'),
('Setor De Ciencias Exatas'),
('Setor De Computação'),
('Setor De Economia');

CREATE TABLE historico(
id_hist SERIAL PRIMARY KEY,
data_inicio DATE NOT NULL,
data_fim DATE NOT NULL
);

INSERT INTO historico(data_inicio, data_fim)
VALUES
('2022/02/01', '2022/09/01'),
('2022/02/10', '2022/09/15'),
('2022/02/04', '2022/09/04');

CREATE TABLE professor(
id_prof SERIAL PRIMARY KEY,
nome VARCHAR(40) NOT NULL,
sobrenome VARCHAR(60),
id_dep INT,
FOREIGN KEY(id_dep) REFERENCES departamento(id_dep)
);

INSERT INTO professor(nome, sobrenome, id_dep)
VALUES
('Caio', 'Henrique', 4),
('Maria', 'Oliveira', 1),
('Jose', 'Silva', 3),
('Fernanda', 'Moura', 2);

CREATE TABLE curso(
id_curso SERIAL PRIMARY KEY,
nome_curso VARCHAR(50) NOT NULL,
id_dep INT,
FOREIGN KEY(id_dep) REFERENCES departamento(id_dep)
);

INSERT INTO curso(nome_curso, id_dep)
VALUES
('Geografia',1),
('Ciencias Biologicas', 1),
('Economia', 4),
('Fisica', 2),
('Matematica', 2),
('Ciencias Da Computação', 3);

CREATE TABLE turma(
	id_turma SERIAL PRIMARY KEY,
	id_curso INTEGER,
	FOREIGN KEY(id_curso) REFERENCES curso(id_curso),
	num_alunos INT NOT NULL,
	periodo VARCHAR(20),
	data_inicio DATE NOT NULL,
	data_fim DATE NOT NULL
);

INSERT INTO turma(id_curso, num_alunos, periodo, data_inicio, data_fim)
VALUES
(3, 25, 'Matutino', '2022/01/05', '2023/01/04'),
(1, 15, 'Matutino', '2022/02/06', '2023/02/09'),
(5, 30, 'Noturno', '2022/05/01', '2022/12/15'),
(4, 18, 'Noturno', '2022/03/09', '2023/03/02'),
(2, 28, 'Matutino', '2022/06/11', '2022/11/11');

CREATE TABLE disciplina(
	id_disciplina SERIAL PRIMARY KEY,
	nome_disciplina VARCHAR(50) NOT NULL,
	num_alunos INTEGER NOT NULL,
	carga_horaria INTEGER NOT NULL,
	descrição TEXT
);

INSERT INTO disciplina(nome_disciplina, num_alunos, carga_horaria, descrição)
VALUES
('Mecânica', 5, 90, 'Disciplina do curso de Física'),
('Calculo I', 6, 85, 'Disciplina do curso de Matemática'),
('Botanica', 10, 90, 'Disciplina do curso de Biologia'),
('Geologia', 9, 79, 'Disciplina do curso de Geografia'),
('Teoria Economica', 15, 90, 'Disciplina do curso de Economia');

CREATE TABLE alunos(
	id_aluno SERIAL PRIMARY KEY,
	nome VARCHAR(45) NOT NULL,
	sobrenome VARCHAR(50),
	sexo CHAR(10),
	nome_mae VARCHAR(66),
	nome_pai VARCHAR(66),
	id_curso INTEGER NOT NULL,
	id_turma INTEGER NOT NULL,
	id_disciplina INTEGER NOT NULL,
	id_hist INTEGER NOT NULL,
	FOREIGN KEY(id_curso) REFERENCES curso(id_curso),
	FOREIGN KEY(id_turma) REFERENCES turma(id_turma),
	FOREIGN KEY(id_disciplina) REFERENCES disciplina(id_disciplina),
	FOREIGN KEY(id_hist) REFERENCES historico(id_hist)
);
	
INSERT INTO alunos(nome, sobrenome, sexo, nome_mae, nome_pai, id_curso, id_turma, id_disciplina,id_hist)
VALUES
('Clarice', 'Silva', 'Feminino', 'Claudia', 'Fernando', 2, 5, 3, 1),
('Bianca', 'Pereira', 'Feminino', 'Fabiana', 'Henrique', 3, 1, 5, 2),
('Mateus', 'Matos', 'Masculino', 'Paula', 'Pedro', 5, 3, 2, 3);

CREATE TABLE endereço_aluno(
	id_aluno INTEGER,
	FOREIGN KEY(id_aluno) REFERENCES alunos(id_aluno),
	cidade VARCHAR(65),
	cep INTEGER,
	complemento TEXT
);
	
INSERT INTO endereço_aluno(id_aluno, cidade, cep, complemento)
VALUES
(1, 'São Paulo', 15448213, 'Rua principal'),
(2, 'São Pualo', 5588441, 'Rua sem nome'),
(3, 'Santos', 1786465, 'Avenida Principal');
	
CREATE TABLE contato_aluno(
	id_contato SERIAL PRIMARY KEY,
	id_aluno INTEGER,
	FOREIGN KEY(id_aluno) REFERENCES alunos(id_aluno),
	whatsapp VARCHAR(20),
	telefone_2 VARCHAR(20),
	email VARCHAR(35)
);

INSERT INTO contato_aluno(id_aluno, whatsapp, telefone_2, email)
VALUES
(1, '1958-4875', '1548-4567', 'claricesilva@gmail.com'),
(2, '546-468774', '5484-868', 'biancapereira@gmail.com'),
(3, '6876-87646', '65687-54846', 'mateusmatos@gmail.com');

CREATE TABLE aluno_disciplina(
	id_aluno INTEGER,
	id_disciplina INTEGER,
	FOREIGN KEY(id_aluno) REFERENCES alunos(id_aluno),
	FOREIGN KEY(id_disciplina) REFERENCES disciplina(id_disciplina)
);

INSERT INTO aluno_disciplina(id_aluno, id_disciplina)
VALUES
(1,3),
(2,5),
(3,2);

CREATE TABLE disci_hist(
	id_disciplina INTEGER,
	id_hist INTEGER,
	FOREIGN KEY(id_disciplina) REFERENCES disciplina(id_disciplina),
	FOREIGN KEY(id_hist) REFERENCES historico(id_hist)
);

INSERT INTO disci_hist(id_disciplina, id_hist)
VALUES
(3,1),
(5,2),
(2,3);

CREATE TABLE curso_disciplina(
	id_curso INTEGER,
	id_disciplina INTEGER,
	FOREIGN KEY(id_curso) REFERENCES curso(id_curso),
	FOREIGN KEY(id_disciplina) REFERENCES disciplina(id_disciplina)
);

INSERT INTO curso_disciplina(id_curso, id_disciplina)
VALUES
(1,4),
(2,3),
(3,5),
(4,1),
(5,2),
(6, 2);

CREATE TABLE professor_disciplina(
	id_prof INTEGER,
	id_disciplina INTEGER,
	FOREIGN KEY(id_prof) REFERENCES professor(id_prof),
	FOREIGN KEY(id_disciplina) REFERENCES disciplina(id_disciplina)
);

INSERT INTO professor_disciplina(id_prof, id_disciplina)
VALUES
(1, 2),
(2, 1),
(3, 5),
(4, 3);
 
-- Adicionando notas na tabela disci_hist

ALTER TABLE disci_hist
ADD COLUMN nota NUMERIC(5,2);

INSERT INTO disci_hist(nota)
VALUES
(9.6),
(7.5),
(8.5);
