### Sistema De Controle Acadêmico.

---


![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white) 

[![PyPi license](https://badgen.net/pypi/license/pip/)](https://pypi.com/project/pip/) 

### Sobre o projeto

Banco de dados relacional para controle acadêmico de uma Universidade. 

### Descrição do projeto

Objetiva otimizar a pesquisa de informações dos docentes com seus respectivos departamentos e disciplinas lecionadas no presente período letivo. 

O sistema também permitirá o monitoramento dos discentes e suas relações com o curso escolhido, sua turma, disciplinas e histórico.

### Status do projeto

Projeto finalizado.

### Tecnologias usadas

- PostgreSQL

Gerenciador de banco de dados relacionais baseado na linguagem SQL.

- PgAdmin4

Interface gráfica para acesso ao PostgreSQL (opcional para uso).

- BrModel

Ferramenta para modelagem gráfica das tabelas.

### Pré-Requisitos

- Ter o PostgreSQL instalado na máquina.


